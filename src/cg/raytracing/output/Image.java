package cg.raytracing.output;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 23:32
 * To change this template use File | Settings | File Templates.
 */
public class Image {
    private BufferedImage im;
    private int w, h;

    public Image(int w, int h) {
        this.w = w;
        this.h = h;
        im = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    }

    public synchronized void setPixel(int x, int y, int color) {
        if (x > w || x < 0 || y > h || y < 0)
            return;
//            throw new IllegalArgumentException("bad x or y");
        im.setRGB(x, y, color);
    }

    public void save(String fileName) throws IOException {
        File f = new File(fileName);
        ImageIO.write(im, "PNG", f);
        return;
    }

    public static void main(String... s) throws IOException {
        Image im = new Image(3096, 3096);
        for (int i = 0; i < 3096; ++i) {
            for (int j = 0; j < 3096; ++j) {
                im.setPixel(i, j, (i * j));
            }
        }
        im.save("C:\\Users\\1\\Dropbox\\cg\\oyt.png");
    }

}
