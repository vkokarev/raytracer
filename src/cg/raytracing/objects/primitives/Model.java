package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 17.03.13
 * Time: 2:45
 * To change this template use File | Settings | File Templates.
 */
public class Model implements Primitive {
    private List<SurfaceTriangle> triangles;
    private Sphere sphere = null;
    private ThreadLocal<SurfaceTriangle> nearestLocal = new InheritableThreadLocal<SurfaceTriangle>();


    public Model(List<SurfaceTriangle> triangles, Vector3 pos, float maxDist) {
        this.triangles = triangles;
        sphere = new Sphere(maxDist, pos);
    }

    @Override
    public float isIntersect(Ray ray) {
        float dist = Float.POSITIVE_INFINITY;
        float minDist = Float.POSITIVE_INFINITY;
        SurfaceTriangle minTr = null;
        if (sphere.isIntersect(ray) != Float.POSITIVE_INFINITY) {
            for (SurfaceTriangle triangle : triangles) {
                dist = triangle.isIntersect(ray);
                if (dist < minDist) {
                    minDist = dist;
                    minTr = triangle;
                }
            }
            nearestLocal.set(minTr);
        }

        return minDist;
    }

//    @Override
//    public float isIntersect(Ray ray) {
//        return 0;  //To change body of implemented methods use File | Settings | File Templates.
//    }

    @Override
    public Vector3 getNormal(Vector3 point) {
        SurfaceTriangle triangle = nearestLocal.get();
        return triangle.getNormal(point);
    }

}
