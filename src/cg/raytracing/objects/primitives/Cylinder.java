package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 06.03.13
 * Time: 22:09
 * To change this template use File | Settings | File Templates.
 */
public class Cylinder implements Primitive {
    private Vector3 start, end;
    private float r;
    private Vector3 dir;
    float h;

    public Cylinder(Vector3 start, Vector3 end, float r) {
        this.start = start;
        this.end = end;
        this.r = r;
        dir = end.minus(start);
        h = dir.getDistToZero();
        dir = dir.getDir();
    }

    @Override
    public float isIntersect(Ray ray) {
        Vector3 ao, aoab, vab;
        float a, b, c;
        float dist = Float.POSITIVE_INFINITY;

        ao = ray.getPosition().minus(start);
        aoab = ao.cross(dir);
        vab = ray.getDir().cross(dir);

        a = vab.mult(vab);
        b = 2 * vab.mult(aoab);
        c = aoab.mult(aoab) - r * r;

        float[] roots = solveQuadraticEquation(a, b, c);
        float distance;

        if (roots[0] != Float.POSITIVE_INFINITY && roots[0] >= 0 && roots[1] >= 0) {
            boolean isFirstOn = isOnCylinder(roots[0], ray);
            boolean isSecondOn = isOnCylinder(roots[1], ray);
            if (isFirstOn && isSecondOn) {
                dist = Math.min(roots[0], roots[1]);
            } else if (isFirstOn) {
                dist = roots[0];
            } else if (isSecondOn) {
                dist = roots[1];
            }
        } else if (roots[0] * roots[1] < 0) {
            dist = Math.max(roots[0], roots[1]);
        }

        return dist;
    }

    private boolean isOnCylinder(float root, Ray ray) {
        Vector3 endPoint = ray.getScaledEndPoint(root);
        Vector3 a = endPoint.minus(start);
        float t = dir.mult(a);

        if (t > h || t < 0)
            return false;

        return true;
    }

    private float[] solveQuadraticEquation(float a, float b, float c) {
        float[] roots = new float[2];
        roots[0] = Float.POSITIVE_INFINITY;
        roots[1] = Float.POSITIVE_INFINITY;
        if (a == 0) {
            roots[0] = -c / b;
        } else {
            float d = b * b - 4 * a * c;

            if (d == 0) {
                roots[0] = (-b) / (2 * a);
                roots[1] = Float.POSITIVE_INFINITY;
            } else if (d > 0) {
                roots[0] = (float)(-b + Math.sqrt(d)) / (2 * a);
                roots[1] = (float)(-b - Math.sqrt(d)) / (2 * a);
            }
        }
        return roots;
    }

    @Override
    public Vector3 getNormal(Vector3 point) {
//        return point.getDir().mult(-1);
        Vector3 ap = point.minus(start);
        Vector3 ab = end.minus(start);
        float t = ab.mult(ap) / ab.mult(ab);
        Vector3 center = new Vector3(start);
        center = center.add(ab).mult(t);
        return point.minus(center).getDir();
    }
}
