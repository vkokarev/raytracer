package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 27.02.13
 * Time: 5:12
 * To change this template use File | Settings | File Templates.
 */
public class Plane implements Primitive {
    private Vector3 n;
    float d;

    public Plane(Vector3 n, float d) {
        this.n = n.getDir();
        this.d = d;
    }

    @Override
    public float isIntersect(Ray ray) {
        float dist = Float.POSITIVE_INFINITY;
        float t = -(n.mult(ray.getPosition()) + d) / n.mult(ray.getDir());
        if (t > 0)
            dist = t;
        return dist;
    }

    @Override
    public Vector3 getNormal(Vector3 point) {
        return n;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
