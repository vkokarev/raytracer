package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;
import cg.raytracing.utils.Pair;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 06.03.13
 * Time: 1:01
 * To change this template use File | Settings | File Templates.
 */
public class Triangle implements Primitive {
    private Vector3[] points;
    private Vector3 n;

    public Triangle(Vector3[] points) {
        if (points == null || points.length != 3)
            throw new IllegalArgumentException("need 3 point for triangle");
        this.points = points;

        n = (points[1].minus(points[0])).cross(points[2].minus(points[0])).getDir();

    }


    @Override
    public float isIntersect(Ray ray) {
        Vector3 pvec = ray.getDir().cross(points[1].minus(points[0]));
        float det =  (points[2].minus(points[0])).mult(pvec);

        if(Math.abs(det) > 1e-8) {
            float invDet = 1 / det;
            Vector3 tvec = ray.getPosition().minus(points[0]);
            float u = invDet * tvec.mult(pvec);
            if (u < 0 || u > 1) {
                return Float.POSITIVE_INFINITY;
            }

            Vector3 qvec = tvec.cross(points[2].minus(points[0]));
            float v = invDet * ray.getDir().mult(qvec);
            if (v < 0 || u + v > 1) {
                return Float.POSITIVE_INFINITY;
            }
            return (points[1].minus(points[0])).mult(qvec) * invDet;
        }


        //Barycentric intersection
//        Vector3 e1 = points[1].minus(points[0]);
//        Vector3 e2 = points[2].minus(points[0]);
//        Vector3 t = ray.getPosition().minus(points[0]);
//        Vector3 p = ray.getDir().cross(e2);
//        Vector3 q = t.cross(e1);
//        Vector3 tuv = new Vector3(new float[]{q.mult(e2), p.mult(t), q.mult(ray.getDir())}).mult(1 / p.mult(e1));
//        if (tuv.getY() >= 0 && tuv.getY() < 1) {
//            if (tuv.getZ() <= (1 - tuv.getY()))
//                return tuv.getX();
//        }

        return Float.POSITIVE_INFINITY;
    }

    @Override
    public Vector3 getNormal(Vector3 point) {
        return n;
    }

    public Pair<Float, Float> getBaricentric(Vector3 point) {
        Vector3 a = points[0];
        Vector3 b = points[1];
        Vector3 c = points[2];
        float den = 1 / ((b.getY() - c.getY()) * (a.getX() - c.getX()) + (c.getX() - b.getX()) * (a.getY() - c.getY()));
        float u = ((b.getY() - c.getY()) * (point.getX() - c.getX()) + (c.getX() - b.getX()) * (point.getY() - c.getY())) * den;
        float v = ((c.getY() - a.getY()) * (point.getX() - c.getX()) + (a.getX() - c.getX()) * (point.getY() - c.getY())) * den;
        return new Pair<Float, Float>(u, v);

    }
}
