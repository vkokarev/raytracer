package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public interface Primitive {
    public float isIntersect(Ray ray);

    public Vector3 getNormal(Vector3 point);
}
