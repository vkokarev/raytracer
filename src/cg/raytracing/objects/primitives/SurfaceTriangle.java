package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;
import cg.raytracing.utils.Pair;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 17.03.13
 * Time: 2:46
 * To change this template use File | Settings | File Templates.
 */
public class SurfaceTriangle implements Primitive {
    private Triangle triangle;
    Vector3[] normals;

    public SurfaceTriangle(Triangle triangle, Vector3[] normals) {
        this.triangle = triangle;
        this.normals = normals;
    }

    @Override
    public float isIntersect(Ray ray) {
        return triangle.isIntersect(ray);
    }

    @Override
    public Vector3 getNormal(Vector3 point) {
        Pair<Float, Float> uv = triangle.getBaricentric(point);
        float u = uv.getFirst();
        float v = uv.getSecond();
        return (normals[1].mult(v)).add(normals[2].mult(1 - u - v)).add(normals[0].mult(u));
    }

    public Pair<Float, Float> getBaricentric(Vector3 point) {
        return triangle.getBaricentric(point);
    }
}
