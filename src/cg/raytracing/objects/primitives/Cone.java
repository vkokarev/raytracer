package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 31.03.13
 * Time: 18:56
 * To change this template use File | Settings | File Templates.
 */
public class Cone implements Primitive {
    private Vector3 top;
    private Vector3 bot;
    private Vector3 dir;
    private float r;
    private float r2;
    private float rph;

    public Cone(Vector3 top, Vector3 bot, float r) {
        this.top = top;
        this.bot = bot;
        this.r = r;
        dir = top.minus(bot);
        r2 = r * r;
        rph = r / dir.getDistToZero();
        dir = dir.getDir();
    }

    @Override
    public float isIntersect(Ray ray) {
        Vector3 CO = ray.getPosition().minus(bot);
        float dirDotAxis = ray.getDir().mult(dir);
        float CODotAxis = CO.mult(dir);

        Vector3 u = ray.getDir().add(dir.mult(-dirDotAxis));
        Vector3 v = CO.add(dir.mult(-CODotAxis));
        float w = CODotAxis * rph;
        float radPerDir = dirDotAxis * rph;

        float a = u.mult(u) - radPerDir * radPerDir;
        float closest = -1.f;
        float root = 0.f;
        if (Math.abs(a) > 1e-5) {
            float b = 2 * (u.mult(v) - w * radPerDir);
            float c = v.mult(v) - w * w;

            float D = b * b - 4 * a * c;

            if (D < 0.f) {
                return Float.POSITIVE_INFINITY;
            }

            D = (float)Math.sqrt(D);

            float denom = 1 / (2 * a);

            root = (-b - D) * denom;
            if (root > 0.f) {
                Vector3 surfacePoint = ray.getScaledEndPoint(root);
                Vector3 toBottom = surfacePoint.minus(bot);
                Vector3 toTop = surfacePoint.minus(top);
                if (dir.mult(toBottom) > 0.f && (dir.mult(-1).mult(toTop) > 0.f)) {
                    closest = root;
                }
            }
            root = (-b + D) * denom;
            if (root > 0.f) {
                Vector3 surfacePoint = ray.getScaledEndPoint(root);
                Vector3 toBottom = surfacePoint.minus(bot);
                Vector3 toTop = surfacePoint.minus(top);
                if (dir.mult(toBottom) > 0.f && (dir.mult(-1).mult(toTop) > 0.f)) {
                    if (closest < 0.f) {
                        closest = root;
                    } else if (root < closest) {
                        closest = root;
                    }
                }
            }
        }

        if (Math.abs(dirDotAxis) < 1e-5) {
            if (closest > 0.f) {
                return closest;
            }

            return Float.POSITIVE_INFINITY;
        }

        root = dir.mult(-1).mult(ray.getPosition().minus(top)) / dirDotAxis;
        if (root > 0.f) {
            Vector3 test = ray.getScaledEndPoint(root).minus(top);
            if (test.mult(test) < r2) {
                if (closest < 0.f) {
                    closest = root;
                } else if (root < closest) {
                    closest = root;
                }
            }
        }

        if (closest > 0.f) {
            return closest;
        }
        return Float.POSITIVE_INFINITY;
    }

    @Override
    public Vector3 getNormal(Vector3 point) {
        Vector3 toBottom = point.minus(top);
        if (Math.abs(dir.mult(toBottom)) < 1e-5 && toBottom.mult(toBottom) < r2) {
            return dir;
        }
        Vector3 approxNorm = point.minus(dir.mult(point.minus(top).mult(dir)).add(top));
        return approxNorm.add(dir.mult(-rph * approxNorm.getDistToZero())).getDir();
    }
}

