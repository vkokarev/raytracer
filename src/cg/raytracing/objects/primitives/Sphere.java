package cg.raytracing.objects.primitives;

import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
public class Sphere implements Primitive {
    private float r;
    private Vector3 center;

    public Sphere(float r, Vector3 center) {
        this.r = r;
        this.center = center;
    }

    public float isIntersect(Ray ray) {

        // Note that locals are named according to the equations in the lecture notes.
        Vector3 L = center.minus(ray.getPosition());
        Vector3 V = ray.getDir();

        float tCA = L.mult(V);

        if (tCA < 0) {
            // In this case the camera is inside the sphere or the sphere center lies
            // behind the ray, which means we have no intersection
            return Float.POSITIVE_INFINITY;
        }


        float LSquare = L.mult(L);

        float dSquare = LSquare - tCA * tCA;
        float radiusSquare = r * r;

        if (dSquare > radiusSquare) {
            // In this case the ray misses the sphere
            return Float.POSITIVE_INFINITY;
        }

        float tHC = (float)Math.sqrt(radiusSquare - dSquare);

        // We now check where the ray originated:
        // Gur: CHECK. LSquare == MathUtils.dotProduct(L, L), can't be smaller
        if (L.mult(L) < LSquare) {
            // The ray originated in the sphere - the intersection is with the exit point
            return (float) (tCA + tHC);
        } else {
            // The ray originated ouside the sphere - the intersection is with the entrance point
            if (tCA - tHC > 0)
                return (float) (tCA - tHC);
            return (float) (tCA + tHC);
        }
    }

    public Vector3 getNormal(Vector3 point) {
        Vector3 normal = point.minus(center);
        return normal.getDir();
    }
}
