package cg.raytracing.objects;

import cg.raytracing.objects.primitives.Primitive;
import cg.raytracing.stuff.Intersection;
import cg.raytracing.stuff.Material;
import cg.raytracing.stuff.Ray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
public class Object {
    private List<Primitive> primitives = new ArrayList<Primitive>();
    Material material;

    public Object() {
    }

    public Object(List<Primitive> primitives, Material material) {
        this.primitives = primitives;
        this.material = material;
    }

    public void setPrimitives(List<Primitive> primitives) {
        this.primitives = primitives;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public void addPrimitive(Primitive primitive) {
        primitives.add(primitive);
    }

    public Intersection isIntersect(Ray ray, List<Primitive> excluded) {
        float dist = Float.POSITIVE_INFINITY;
        Primitive intersection = null;
        for (Primitive primitive : primitives) {
            float cdist = primitive.isIntersect(ray);
            if (cdist > 0 && cdist < dist && (excluded == null || !excluded.contains(primitive))) {
                dist = cdist;
                intersection = primitive;
            }
        }
        return new Intersection(this, intersection, ray, dist);
//        return new Intersection(this, intersection, 10  );
    }

}
