package cg.raytracing.objects;

import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
public class Camera {
    private Vector3 pos;
    private Vector3 lootAt;
    private Vector3 up;
    private Vector3 dir;
    private float fovX;
    private float fovY;
    private float distToNearPlane;
    private Vector3 rightDirection;
    private final Vector3 viewplaneUp;

    public Camera(Vector3 pos, Vector3 lootAt, Vector3 up, float fovX, float fovY, float distToNearPlane) {
        this.pos = pos;
        this.lootAt = lootAt;
        this.up = up.getDir();
        this.fovX = fovX;
        this.fovY = fovY;
        this.distToNearPlane = distToNearPlane;
        dir = lootAt.minus(pos).getDir();

        rightDirection = up.cross(dir).getDir().mult(-1);
        viewplaneUp = rightDirection.cross(dir).getDir();
    }

    public Vector3 getRightDirection() {
        return rightDirection;
    }

    public Vector3 getViewplaneUp() {
        return viewplaneUp;
    }

    public Vector3 getPos() {
        return pos;
    }

    public void setPos(Vector3 pos) {
        this.pos = pos;
    }

    public Vector3 getLootAt() {
        return lootAt;
    }

    public void setLootAt(Vector3 lootAt) {
        this.lootAt = lootAt;
    }

    public Vector3 getUp() {
        return up;
    }

    public void setUp(Vector3 up) {
        this.up = up;
    }

    public float getFovX() {
        return fovX;
    }

    public void setFovX(float fovX) {
        this.fovX = fovX;
    }

    public float getFovY() {
        return fovY;
    }

    public void setFovY(float fovY) {
        this.fovY = fovY;
    }

    public float getDistToNearPlane() {
        return distToNearPlane;
    }

    public void setDistToNearPlane(float distToNearPlane) {
        this.distToNearPlane = distToNearPlane;
    }

    public Vector3 getDir() {
        return dir;
    }
}
