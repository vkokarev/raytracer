package cg.raytracing.objects.csg.operation;

import cg.raytracing.objects.csg.CSGTree;
import cg.raytracing.stuff.Intersection;
import cg.raytracing.stuff.Ray;
import cg.raytracing.utils.GlobalSettings;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.04.13
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */
public abstract class CSGOperation {
    public abstract Intersection apply(CSGTree left, CSGTree right, Ray ray);
    protected Intersection getFarInt(Intersection minInt, CSGTree node, Ray origin) {
        Intersection farInt = minInt;
        Intersection max = Intersection.NOT_VALID;
        float dist = minInt.getDist();
        do {
            farInt = node.getIntersection(new Ray(farInt.getIntersectionPoint().add(origin.getDir().mult(GlobalSettings.SUPERMAGICEPSILONCONSTANTTHATHASREALLYBIGNAME)), origin.getDir()));
//            farInt = node.getIntersection(origin);
            if (farInt.isValid()) {
                max = farInt;
                dist += farInt.getDist();
            }
        } while (farInt.isValid());
        if (max.isValid())
            max.setDist(dist);
        return max;
    }
}
