package cg.raytracing.objects.csg.operation;

import cg.raytracing.objects.csg.CSGTree;
import cg.raytracing.stuff.Intersection;
import cg.raytracing.stuff.Ray;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.04.13
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public class CSGDiff extends CSGOperation {
    @Override
    public Intersection apply(CSGTree left, CSGTree right, Ray ray) {
        Intersection leftMinInt = left.getIntersection(ray);
        Intersection rightMinInt = right.getIntersection(ray);
        if (!leftMinInt.isValid()){
            return Intersection.NOT_VALID;
        }
        if (!rightMinInt.isValid()) {
            return leftMinInt;
        }
        Intersection leftMaxInt = getFarInt(leftMinInt, left, ray);
        Intersection rightMaxInt = getFarInt(rightMinInt, left, ray);

        if (leftMaxInt == null)
            leftMaxInt = Intersection.NOT_VALID;
        if (rightMaxInt == null)
            rightMaxInt = Intersection.NOT_VALID;

        if (rightMaxInt.getDist() < leftMinInt.getDist()) {
            return leftMaxInt; //todo??????
        }

        if (leftMaxInt.getDist() < rightMinInt.getDist()) {

            return leftMinInt; //todo????
        }

        if (leftMinInt.getDist() < rightMinInt.getDist()) {
            return leftMinInt;
        }

        if (rightMaxInt.getDist() < leftMaxInt.getDist()) {
            rightMaxInt.setNormalCoeff(-1);
            return rightMaxInt;
            //todo minus normal
        }

        return new Intersection(null, null, ray, Float.MAX_VALUE);
    }
}
