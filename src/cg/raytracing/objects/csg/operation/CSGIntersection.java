package cg.raytracing.objects.csg.operation;

import cg.raytracing.objects.csg.CSGTree;
import cg.raytracing.stuff.Intersection;
import cg.raytracing.stuff.Ray;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.04.13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
public class CSGIntersection extends CSGOperation {

    @Override
    public Intersection apply(CSGTree left, CSGTree right, Ray ray) {
        Intersection leftInt = left.getIntersection(ray);
        Intersection rightInt = right.getIntersection(ray);
        if (leftInt.isValid() && rightInt.isValid()) {
            Intersection farLeftInt = getFarInt(leftInt, left, ray);
            Intersection farRightInt = getFarInt(rightInt, right, ray);

            if (leftInt.getDist() < rightInt.getDist() && farLeftInt.getDist() > rightInt.getDist()) {
                return rightInt;
            } else if (rightInt.getDist() < leftInt.getDist() && farRightInt.getDist() > leftInt.getDist()) {
                return leftInt;
            }
        }
        return Intersection.NOT_VALID;
    }
}
