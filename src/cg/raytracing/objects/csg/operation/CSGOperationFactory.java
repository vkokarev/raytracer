package cg.raytracing.objects.csg.operation;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.04.13
 * Time: 19:01
 * To change this template use File | Settings | File Templates.
 */
public class CSGOperationFactory {
    private static final CSGOperation union = new CSGUnion();
    private static final CSGOperation diff = new CSGDiff();
    private static final CSGOperation intersect = new CSGIntersection();
    public static CSGOperation getInstance(String name) {
        if ("union".equals(name))
            return union;
        else if ("diff".equals(name))
            return diff;
        else if ("intersection".equals(name))
            return intersect;
        throw new IllegalArgumentException("bad name for CSG operation " + name);
    }
}
