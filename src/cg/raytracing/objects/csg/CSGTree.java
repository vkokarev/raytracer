package cg.raytracing.objects.csg;

import cg.raytracing.objects.Object;
import cg.raytracing.objects.csg.operation.CSGOperation;
import cg.raytracing.objects.primitives.Primitive;
import cg.raytracing.stuff.Intersection;
import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.04.13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public class CSGTree implements Primitive {
    private CSGTree left, right;
    private CSGOperation operation = null;
    private Object obj;
    private static final ThreadLocal<Intersection> intersection = new ThreadLocal<Intersection>();

    public Intersection getIntersection(Ray ray) {
        //is leaf?
        if (obj != null) {
            return obj.isIntersect(ray, null);
        } else {
            return operation.apply(left, right, ray);
        }
    }

    @java.lang.Override
    public float isIntersect(Ray ray) {
        float dist = Float.MAX_VALUE;
        //is leaf?
        try{
        if (obj != null) {
            Intersection intersection = obj.isIntersect(ray, null);
            if (intersection.isValid()) {
                this.intersection.set(intersection);
                dist = intersection.getDist();
            }
        } else {
            Intersection intersection = operation.apply(left, right, ray);
            if (intersection.isValid())
                this.intersection.set(intersection);
            dist = intersection.getDist();
        }
        }catch (Throwable t){
            t.printStackTrace();
        }
        return dist;
    }

    @java.lang.Override
    public Vector3 getNormal(Vector3 point) {
        Intersection intersection = CSGTree.intersection.get();
        if (intersection != null && intersection.isValid() && intersection.getPrimitive() != null)
            return intersection.getPrimitive().getNormal(point).mult(intersection.getNormalCoeff());
        return Vector3.ZERO;
    }

    public void appendRight(CSGTree rt) {
        this.right = rt;
    }

    public void appendLeft(CSGTree lt) {
        this.left = lt;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public void setOperation(CSGOperation operation) {
        this.operation = operation;
    }

}
