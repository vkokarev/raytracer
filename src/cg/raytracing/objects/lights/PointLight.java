package cg.raytracing.objects.lights;

import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 18:40
 * To change this template use File | Settings | File Templates.
 */
public class PointLight extends Light {
    private float[] att = {1, 0.2f, 0.02f};

    public PointLight(Vector3 position, Color ambient, Color diffuse, Color specular, Vector3 att) {
        super(position, ambient, diffuse, specular);
        this.att = new float[]{att.getX(), att.getY(), att.getZ()};
    }

    @Override
    public Ray rayFromPoint(Vector3 intersectionPoint) {
        return new Ray(intersectionPoint, position.minus(intersectionPoint).getDir());
    }

    @Override
    public Color getCoeff(Vector3 intersectionPoint) {
        float d = intersectionPoint.getDistTo(position);
        float attenuateion = 1 / (att[2] * d * d + att[1] * d + att[0]);
        return Color.valueOf(new Vector3(ambient.getAsArray()).mult(attenuateion));
    }
}
