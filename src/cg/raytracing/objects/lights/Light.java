package cg.raytracing.objects.lights;

import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
public abstract class Light {
    protected Vector3 position;
    protected Color ambient;
    protected Color diffuse;
    protected Color specular;

    public Light(Vector3 position, Color ambient, Color diffuse, Color specular) {
        this.position = position;
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
    }

    public abstract Ray rayFromPoint(Vector3 intersectionPoint);

    public Vector3 getPosition() {
        return position;
    }

    public Color getAmbient() {
        return ambient;
    }

    public abstract Color getCoeff(Vector3 intersectionPoint);
}
