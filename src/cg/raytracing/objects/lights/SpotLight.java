package cg.raytracing.objects.lights;

import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 30.03.13
 * Time: 22:36
 * To change this template use File | Settings | File Templates.
 */
public class SpotLight extends Light {
    private float[] att = {1f, 0.02f, 0.02f};
    private Vector3 dir;
    private float cosHalfUmbraAngle;
    private float cosHalfPenumbraAngle;
    private float spotlightFalloff;

    public SpotLight(Vector3 position, Color ambient, Color diffuse, Color specular, float spotlightFalloff, Vector3 dir, Vector3 att, float penumbraAngle, float umbraAngle) {
        super(position, ambient, diffuse, specular);
        this.spotlightFalloff = spotlightFalloff;
        this.dir = dir.getDir();
        this.att = new float[]{att.getX(), att.getY(), att.getZ()};
        cosHalfPenumbraAngle = (float)Math.cos(penumbraAngle * Math.PI / 180);
        cosHalfUmbraAngle = (float)Math.cos(umbraAngle * Math.PI / 180);
    }

    @Override
    public Ray rayFromPoint(Vector3 intersectionPoint) {
        Vector3 back = (position.minus(intersectionPoint)).getDir();
        float cos = -back.mult(dir);
        if (cos > cosHalfPenumbraAngle) {
            return new Ray(intersectionPoint, back);
        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Color getCoeff(Vector3 intersectionPoint) {
        float d = intersectionPoint.getDistTo(position);
        float attenuation = 1 / (att[2] * d * d + att[1] * d + att[0]);
        Vector3 back = (intersectionPoint.minus(position)).getDir();
        float cos = back.mult(dir);
        float spotAttenuation = 0.f;
        if (cos > cosHalfUmbraAngle) {
            spotAttenuation = 1.f;
        } else if (cos < cosHalfPenumbraAngle) {
            spotAttenuation = 0.f;
        } else {
            float factor = (cos - cosHalfPenumbraAngle) / (cosHalfUmbraAngle - cosHalfPenumbraAngle);
            spotAttenuation = (float)Math.pow(factor, spotlightFalloff);
        }
        attenuation *= spotAttenuation;
        return Color.valueOf(new Vector3(ambient.getAsArray()).mult(attenuation));
    }
}
