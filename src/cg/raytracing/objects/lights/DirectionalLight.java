package cg.raytracing.objects.lights;

import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Ray;
import cg.raytracing.stuff.Vector3;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public class DirectionalLight extends Light {
    private Vector3 dir;
    private Vector3 back;


    public DirectionalLight(Vector3 position, Vector3 dir, Color ambient, Color diffuse, Color specular) {
        super(position, ambient, diffuse, specular);
        this.dir = dir;
        back = dir.mult(-1);
    }

    @Override
    public Ray rayFromPoint(Vector3 intersectionPoint) {
        return new Ray(intersectionPoint, back);
    }

    @Override
    public Color getCoeff(Vector3 intersectionPoint) {
        return new Color(1, 1, 1);
    }
}
