package cg.raytracing.objects;

import cg.raytracing.stuff.Vector3;
import cg.raytracing.utils.Matrix4;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */
public class World {
    private Scene scene;
    private Camera camera;

    private Matrix4 view;
    private Matrix4 proj;
    private Matrix4 pv;


    public Matrix4 getPv() {
        return pv;
    }


    public World(Scene scene, Camera camera) {
        this.scene = scene;
        this.camera = camera;
        proj = perspectiveFovRH();
        view = lookAtRH();
        pv = Matrix4.valueOf(view.times(proj));
    }

    private Matrix4 perspectiveFovRH() {
        float h = (float)(1. / Math.tan(camera.getFovY() / 2));
        float w = h * 16 / 9;

        return new Matrix4(new float[][]{
                {w, 0, 0, 0},
                {0, h, 0, 0},
                {0, 0, 10000 / (camera.getDistToNearPlane() - 10000), -1},
                {0, 0, camera.getDistToNearPlane() * 10000 / (camera.getDistToNearPlane() - 10000), 0}
        });
    }

    private Matrix4 lookAtRH() {
        Vector3 zaxis = camera.getPos().minus(camera.getLootAt()).getDir();
        Vector3 xaxis = camera.getUp().cross(zaxis).getDir();
        Vector3 yaxis = zaxis.cross(xaxis);

        return new Matrix4(new float[][]{{xaxis.getX(), yaxis.getX(), zaxis.getX(), 0},
                {xaxis.getY(), yaxis.getY(), zaxis.getY(), 0},
                {xaxis.getZ(), yaxis.getZ(), zaxis.getZ(), 0},
                {xaxis.mult(camera.getPos()), yaxis.mult(camera.getPos()), zaxis.mult(camera.getPos()), 1}});
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Camera getCamera() {
        return camera;
    }
}
