package cg.raytracing.objects;

import cg.raytracing.objects.lights.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 15:24
 * To change this template use File | Settings | File Templates.
 */
public class Scene {
    private List<Object> objects = new ArrayList<Object>();
    private List<Light> lights = new ArrayList<Light>();

    public Scene() {
    }

    public Scene(List<Object> objects, List<Light> lights) {
        this.objects = objects;
        this.lights = lights;
    }

    public List<Object> getObjects() {
        return objects;
    }

    public List<Light> getLights() {
        return lights;
    }

    public void addLight(Light light) {
        lights.add(light);
    }

    public void addObject(Object obj) {
        objects.add(obj);
    }

}
