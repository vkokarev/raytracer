package cg.raytracing.stuff;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */
public class Material {
    private Color ambient;
    private Color diffuse;
    private Color specular;
    private float specularPow;
    private float refractionCoeff;
    private float illuminationFactor;
    private float refrationFactor;
    private float reflectionFactor;


    public Material() {
    }

    public Material(Color ambient, Color diffuse, Color specular, float specularPow, float refractionCoeff, float illuminationFactor, float refrationFactor, float reflectionFactor) {
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        this.specularPow = specularPow;
        this.refractionCoeff = refractionCoeff;
        this.illuminationFactor = illuminationFactor;
        this.refrationFactor = refrationFactor;
        this.reflectionFactor = reflectionFactor;
    }

    public Material(Color ambient, Color diffuse, Color specular, float specularPow) {
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        this.specularPow = specularPow;
    }

    public Color getAmbient() {
        return ambient;
    }

    public void setAmbient(Color ambient) {
        this.ambient = ambient;
    }

    public Color getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(Color diffuse) {
        this.diffuse = diffuse;
    }

    public Color getSpecular() {
        return specular;
    }

    public void setSpecular(Color specular) {
        this.specular = specular;
    }

    public float getSpecularPow() {
        return specularPow;
    }

    public void setSpecularPow(float specularPow) {
        this.specularPow = specularPow;
    }

    public float getRefractionCoeff() {
        return refractionCoeff;
    }

    public void setRefractionCoeff(float refractionCoeff) {
        this.refractionCoeff = refractionCoeff;
    }

    public float getIlluminationFactor() {
        return illuminationFactor;
    }

    public void setIlluminationFactor(float illuminationFactor) {
        this.illuminationFactor = illuminationFactor;
    }

    public float getRefrationFactor() {
        return refrationFactor;
    }

    public void setRefrationFactor(float refrationFactor) {
        this.refrationFactor = refrationFactor;
    }

    public boolean reflects() {
        return reflectionFactor > 0;
    }

    public float getReflectionFactor() {
        return reflectionFactor;
    }

    public void setReflectionFactor(float reflectionFactor) {
        this.reflectionFactor = reflectionFactor;
    }

    public boolean refracts() {
        return refrationFactor > 0;
    }
}
