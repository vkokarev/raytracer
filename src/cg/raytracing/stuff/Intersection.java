package cg.raytracing.stuff;

import cg.raytracing.objects.Object;
import cg.raytracing.objects.primitives.Primitive;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */
public class Intersection {
    public static final Intersection NOT_VALID = new Intersection(null, null, null, Float.MAX_VALUE);
    private Object object;
    private Primitive primitive;
    private Ray ray;
    private float dist;
    private float normalCoeff = 1;


    public Intersection(Object object, Primitive primitive, Ray ray, float dist) {
        this.object = object;
        this.primitive = primitive;
        this.ray = ray;
        this.dist = dist;
    }

    public Vector3 getIntersectionPoint() {
        return ray.getScaledEndPoint(dist);
    }

    public Ray getRay() {
        return ray;
    }

    public void setRay(Ray ray) {
        this.ray = ray;
    }

    public Primitive getPrimitive() {
        return primitive;
    }

    public float getDist() {
        return dist;
    }

    public boolean isValid() {
        return (object != null) && (primitive != null) && (dist != Float.POSITIVE_INFINITY) && (ray != null);
    }

    public Object getObject() {
        return object;
    }

    public void setDist(float dist) {
        this.dist = dist;
    }

    public float getNormalCoeff() {
        return normalCoeff;
    }

    public void setNormalCoeff(float normalCoeff) {
        this.normalCoeff = normalCoeff;
    }
}
