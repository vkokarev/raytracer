package cg.raytracing.stuff;

import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
public class Vector3 {
    public static final Vector3 ZERO = new Vector3(new float[]{0, 0, 0});
    private float pos[];

    public Vector3(float x, float y, float z) {
        pos = new float[]{x, y, z};
    }

    public Vector3(float[] coordinates) {
        if (coordinates.length < 3)
            throw new IllegalArgumentException("point must have just 3 coordinates");
        pos = Arrays.copyOf(coordinates, 3);
        return;
    }

    public Vector3(Vector3 v) {
        pos = new float[3];
        pos = Arrays.copyOf(v.pos, 3);
    }


    public Vector3 mult(float a) {
        return new Vector3(a * pos[0], a * pos[1], a * pos[2]);
    }

    public float getDistToZero() {
        return getDistTo(ZERO);
    }

    public float getDistTo(Vector3 vector3) {
        float[] pos1 = vector3.pos;
        return (float) Math.sqrt((pos[0] - pos1[0]) * (pos[0] - pos1[0]) +
                (pos[1] - pos1[1]) * (pos[1] - pos1[1]) +
                (pos[2] - pos1[2]) * (pos[2] - pos1[2]));
    }

    public float mult(Vector3 vector3) {
        float[] pos1 = vector3.pos;
        return pos[0] * pos1[0] + pos[1] * pos1[1] + pos[2] * pos1[2];
    }

    public Vector3 getDir() {
        float len = getDistToZero();
        if (len != 0) {
            return new Vector3(pos[0] / len, pos[1] / len, pos[2] / len);
        } else
            return new Vector3(0, 0, 0);
    }


    public float getX() {
        return pos[0];
    }

    public void setX(float x) {
        pos[0] = x;
    }

    public float getY() {
        return pos[1];
    }

    public void setY(float y) {
        pos[1] = y;
    }

    public float getZ() {
        return pos[2];
    }

    public void setZ(float z) {
        pos[2] = z;
    }

    public Vector3 minus(Vector3 vector3) {
        float[] pos1 = vector3.pos;
        return new Vector3(pos[0] - pos1[0], pos[1] - pos1[1], pos[2] - pos1[2]);
    }

    public Vector3 multComponent(Vector3 coeff) {
        float[] pos1 = coeff.pos;
        return new Vector3(pos[0] * pos1[0], pos[1] * pos1[1], pos[2] * pos1[2]);
    }

    public Vector3 cross(Vector3 xaxis) {
        float[] d1 = pos;
        float[] d2 = xaxis.pos;
        float[] result = {(d1[1] * d2[2]) - (d1[2] * d2[1]), (d1[2] * d2[0]) - (d1[0] * d2[2]), (d1[0] * d2[1]) - (d1[1] * d2[0])};
        return new Vector3(result);
    }

    public Vector3 add(Vector3 vector) {
        float[] pos1 = vector.pos;
        return new Vector3(pos[0] + pos1[0], pos[1] + pos1[1], pos[2] + pos1[2]);
    }
}

