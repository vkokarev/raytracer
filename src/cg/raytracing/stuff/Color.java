package cg.raytracing.stuff;

import cg.raytracing.utils.GlobalSettings;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 17:12
 * To change this template use File | Settings | File Templates.
 */
public class Color {
    float r, g, b;

    public Color(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public float getG() {
        return g;
    }

    public void setG(float g) {
        this.g = g;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public Color add(Color c) {
        return new Color(r + c.r, g + c.g, b + c.b);
    }

    private float gammaCorrection(float color) {
        return (float)(color < 0.0031308 ? 12.92 * color : 1.055f * Math.pow(color, 0.4166667f) - 0.055f);
    }
    public int normolize() {
        if (GlobalSettings.GAMMA_CORRECTION) {
            r = gammaCorrection(r);
            g = gammaCorrection(g);
            b = gammaCorrection(b);
        }
        int colorInt = Math.min(255, (int) Math.round(r * 255)) << 16 & 0xFF0000 |
                Math.min(255, (int) Math.round(g * 255)) << 8 & 0xFF00 |
                Math.min(255, (int) Math.round(b * 255));

        return colorInt;
    }

    public void norm() {
        r = Math.min(1, r);
        g = Math.min(1, g);
        b = Math.min(1, b);
    }

    public float[] getAsArray() {
        return new float[]{r, g, b};
    }

    public static Color valueOf(Vector3 vec) {
        return new Color(vec.getX(), vec.getY(), vec.getZ());
    }

    public void mult(float k) {
        r *= k;
        g *= k;
        b *= k;
    }

}
