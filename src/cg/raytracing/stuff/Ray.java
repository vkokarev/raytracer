package cg.raytracing.stuff;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
public class Ray {
    private Vector3 position;
    private Vector3 dir;

    public Ray(Vector3 position, Vector3 dir) {
        this.position = position;
        this.dir = dir.getDir();
    }

    public Ray(float[] position, float[] dir) {
        this.position = new Vector3(position);
        this.dir = new Vector3(dir);
    }

    public void normolize() {
        dir = dir.getDir();
    }

    public float scalarMult(Ray ray) {
        return dir.mult(ray.dir);
    }

    private float getNorm(Vector3 vector3) {
        return vector3.getDistToZero();
    }

    public Vector3 getPosition() {
        return new Vector3(position);
    }

    public void setPosition(Vector3 position) {
        this.position = position;
    }

    public Vector3 getDir() {
        return new Vector3(dir);
    }

    public void setDir(Vector3 dir) {
        this.dir = dir.getDir();
    }


    public Vector3 getScaledEndPoint(float dist) {
        Vector3 endPoint = new Vector3(this.dir);
        endPoint = endPoint.mult(dist);
        return endPoint.add(position);
    }

    public Vector3 getReflected(Vector3 normal) {
        float proj = dir.mult(normal);
        normal = normal.mult(2 * proj);
        return new Vector3(-dir.getX() + normal.getX(), -dir.getY() + normal.getY(), -dir.getZ() + normal.getZ());
    }
}
