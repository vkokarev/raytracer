package cg.raytracing.builders.lights;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.lights.Light;
import cg.raytracing.objects.lights.SpotLight;
import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 30.03.13
 * Time: 22:51
 * To change this template use File | Settings | File Templates.
 */
public class SpotLightBuilder {
    public static Light build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        Vector3 pos = VectorBuilder.build((Node) xPath.evaluate("./pos", node, XPathConstants.NODE));
        Vector3 ambient = VectorBuilder.build((Node) xPath.evaluate("./ambient_emission", node, XPathConstants.NODE));
        Vector3 diffuse = VectorBuilder.build((Node) xPath.evaluate("./diffuse_emission", node, XPathConstants.NODE));
        Vector3 specular = VectorBuilder.build((Node) xPath.evaluate("./specular_emission", node, XPathConstants.NODE));
        Vector3 dir = VectorBuilder.build((Node) xPath.evaluate("./dir", node, XPathConstants.NODE));
        Vector3 att = VectorBuilder.build((Node) xPath.evaluate("./attenuation", node, XPathConstants.NODE));
        float penumbraAngle = Float.valueOf((String)xPath.evaluate("./penumbra/@angle", node, XPathConstants.STRING));
        float umbraAngle = Float.valueOf((String)xPath.evaluate("./umbra/@angle", node, XPathConstants.STRING));
        float spotlightFalloff = Float.valueOf((String)xPath.evaluate("./falloff/@value", node, XPathConstants.STRING));
        return new SpotLight(pos, Color.valueOf(ambient), Color.valueOf(diffuse), Color.valueOf(specular), spotlightFalloff, dir, att, penumbraAngle, umbraAngle);
    }
}
