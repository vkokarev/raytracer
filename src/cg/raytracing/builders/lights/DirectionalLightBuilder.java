package cg.raytracing.builders.lights;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.lights.DirectionalLight;
import cg.raytracing.objects.lights.Light;
import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.03.13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
public class DirectionalLightBuilder {
    public static Light build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        Vector3 dir = VectorBuilder.build((Node) xPath.evaluate("./dir", node, XPathConstants.NODE));
        Vector3 ambient = VectorBuilder.build((Node) xPath.evaluate("./ambient_emission", node, XPathConstants.NODE));
        Vector3 diffuse = VectorBuilder.build((Node) xPath.evaluate("./diffuse_emission", node, XPathConstants.NODE));
        Vector3 specular = VectorBuilder.build((Node) xPath.evaluate("./specular_emission", node, XPathConstants.NODE));

        return new DirectionalLight(null, dir, Color.valueOf(ambient), Color.valueOf(diffuse), Color.valueOf(specular));
    }
}
