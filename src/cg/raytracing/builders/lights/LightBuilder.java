package cg.raytracing.builders.lights;

import cg.raytracing.objects.lights.Light;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */
public class LightBuilder {
    public static Light build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        String type = (String) xPath.evaluate("@type", node, XPathConstants.STRING);
        if ("point".equals(type)) {
            return PointLightBuilder.build(node);
        } else if ("directional".equals(type)) {
            return DirectionalLightBuilder.build(node);
        } else if ("spot".equals(type)) {
            return SpotLightBuilder.build(node);
        } else {
            throw new IllegalArgumentException("can't init light type " + type);
        }
    }
}
