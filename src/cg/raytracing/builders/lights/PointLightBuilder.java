package cg.raytracing.builders.lights;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.lights.Light;
import cg.raytracing.objects.lights.PointLight;
import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.03.13
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
class PointLightBuilder {
    public static Light build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        Vector3 pos = VectorBuilder.build((Node) xPath.evaluate("./pos", node, XPathConstants.NODE));
        Vector3 ambient = VectorBuilder.build((Node) xPath.evaluate("./ambient_emission", node, XPathConstants.NODE));
        Vector3 diffuse = VectorBuilder.build((Node) xPath.evaluate("./diffuse_emission", node, XPathConstants.NODE));
        Vector3 specular = VectorBuilder.build((Node) xPath.evaluate("./specular_emission", node, XPathConstants.NODE));
        Vector3 att = VectorBuilder.build((Node) xPath.evaluate("./attenuation", node, XPathConstants.NODE));
        return new PointLight(pos, Color.valueOf(ambient), Color.valueOf(diffuse), Color.valueOf(specular), att);
    }
}
