package cg.raytracing.builders;

import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 22:14
 * To change this template use File | Settings | File Templates.
 */
public class VectorBuilder {
    public static Vector3 build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        float x = Float.valueOf((String) xPath.evaluate("./@x", node, XPathConstants.STRING));
        float y = Float.valueOf((String) xPath.evaluate("./@y", node, XPathConstants.STRING));
        float z = Float.valueOf((String) xPath.evaluate("./@z", node, XPathConstants.STRING));
        return new Vector3(x, y, z);
    }
}
