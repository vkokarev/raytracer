package cg.raytracing.builders;

import cg.raytracing.builders.lights.LightBuilder;
import cg.raytracing.builders.object.ObjectBuilder;
import cg.raytracing.objects.Camera;
import cg.raytracing.objects.Object;
import cg.raytracing.objects.Scene;
import cg.raytracing.objects.World;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;


/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 18:53
 * To change this template use File | Settings | File Templates.
 */
public class WorldBuilder {
    public static World build(File file) throws IOException, SAXException, IOException, ParserConfigurationException, XPathExpressionException {
        System.out.println("Start building world");
        long startTime = System.currentTimeMillis();

        System.out.print("Openning xml file... ");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();

        System.out.println("[OK]");

        Node settings = (Node) xPath.evaluate("/scene/global", doc, XPathConstants.NODE);
        Node cameraNode = (Node) xPath.evaluate("/scene/camera", doc, XPathConstants.NODE);
        NodeList objectNodes = (NodeList) xPath.evaluate("/scene/object", doc, XPathConstants.NODESET);
        NodeList lightNodes = (NodeList) xPath.evaluate("/scene/light", doc, XPathConstants.NODESET);

        GlobalSettingsBuilder.build(settings);
        Camera camera = CameraBuilder.build(cameraNode);

        Scene scene = new Scene();
        int len = objectNodes.getLength();
        System.out.println("Creating " + len + " objects");
        for (int i = 0; i < len; ++i) {
            scene.addObject(ObjectBuilder.build(objectNodes.item(i)));
        }

        len = lightNodes.getLength();
        System.out.println("Creating " + len + " lights");
        for (int i = 0; i < len; ++i) {
            scene.addLight(LightBuilder.build(lightNodes.item(i)));
        }

        for (Object obj : scene.getObjects()) {
            if (obj.getMaterial() == null) {
                System.out.println("Error: no material for " + obj.toString());
                throw new IllegalArgumentException("no material");
            }
        }

        System.out.println("World build in " + (System.currentTimeMillis() - startTime) + " ms");

        return new World(scene, camera);
    }
}
