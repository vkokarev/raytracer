package cg.raytracing.builders.object;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.stuff.Color;
import cg.raytracing.stuff.Material;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
class MaterialBuilder {
    public static Material build(Node node) throws XPathExpressionException {
        if (node == null)
            return null;
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        Vector3 ambient = VectorBuilder.build((Node) xPath.evaluate("./ambient", node, XPathConstants.NODE));
        Vector3 diffuse = VectorBuilder.build((Node) xPath.evaluate("./diffuse", node, XPathConstants.NODE));
        Vector3 specular = VectorBuilder.build((Node) xPath.evaluate("./specular", node, XPathConstants.NODE));
        float illuminationFactor = Float.valueOf((String) xPath.evaluate("./illumination_factors/@illumination_factor", node, XPathConstants.STRING));
        float refractionFactor = Float.valueOf((String) xPath.evaluate("./illumination_factors/@refraction_factor", node, XPathConstants.STRING));
        float reflectionFactor = Float.valueOf((String) xPath.evaluate("./illumination_factors/@reflection_factor", node, XPathConstants.STRING));

        float specularPow = Float.valueOf((String) xPath.evaluate("./specular_power/@power", node, XPathConstants.STRING));
        float refractionCoeff = Float.valueOf((String) xPath.evaluate("./refraction_coeff/@theta", node, XPathConstants.STRING));

        return new Material(Color.valueOf(ambient), Color.valueOf(diffuse), Color.valueOf(specular), specularPow, refractionCoeff, illuminationFactor, refractionFactor, reflectionFactor);

    }
}
