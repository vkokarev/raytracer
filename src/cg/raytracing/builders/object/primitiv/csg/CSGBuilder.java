package cg.raytracing.builders.object.primitiv.csg;

import cg.raytracing.builders.object.ObjectBuilder;
import cg.raytracing.objects.csg.CSGTree;
import cg.raytracing.objects.csg.operation.CSGOperationFactory;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.04.13
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
public class CSGBuilder {
    public static CSGTree build(Node node) throws XPathExpressionException, IOException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        String operation = (String)xPath.evaluate("./operation/@type", node, XPathConstants.STRING);
        Node obj = (Node)xPath.evaluate("./object", node, XPathConstants.NODE);
        CSGTree me = new CSGTree();

        if (operation != null && !operation.isEmpty()) {
            Node left = (Node)xPath.evaluate("./operation/left", node, XPathConstants.NODE);
            Node right = (Node)xPath.evaluate("./operation/right", node, XPathConstants.NODE);
            CSGTree lt = build(left);
            CSGTree rt = build(right);
            me.appendLeft(lt);
            me.appendRight(rt);
            me.setOperation(CSGOperationFactory.getInstance(operation));
        } else {
            cg.raytracing.objects.Object object = ObjectBuilder.build(obj);
            me.setObj(object);
        }
        return me;
    }
}
