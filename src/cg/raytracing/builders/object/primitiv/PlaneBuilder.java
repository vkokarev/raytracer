package cg.raytracing.builders.object.primitiv;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.primitives.Plane;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 06.03.13
 * Time: 0:43
 * To change this template use File | Settings | File Templates.
 */
public class PlaneBuilder {
    public static Plane build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        float d = Float.valueOf((String) xPath.evaluate("./D/@d", node, XPathConstants.STRING));
        Vector3 n = VectorBuilder.build((Node) xPath.evaluate("./normal", node, XPathConstants.NODE));
        return new Plane(n, d);
    }
}
