package cg.raytracing.builders.object.primitiv;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.primitives.Cone;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 31.03.13
 * Time: 19:37
 * To change this template use File | Settings | File Templates.
 */
public class ConeBuilder {
    public static Cone build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        float r = Float.valueOf((String) xPath.evaluate("./radius/@r", node, XPathConstants.STRING));
        Vector3 top = VectorBuilder.build((Node) xPath.evaluate("./top", node, XPathConstants.NODE));
        Vector3 bot = VectorBuilder.build((Node)xPath.evaluate("./bottom", node, XPathConstants.NODE));
        return new Cone(bot, top, r);
    }
}
