package cg.raytracing.builders.object.primitiv;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.primitives.Primitive;
import cg.raytracing.objects.primitives.Sphere;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 22:31
 * To change this template use File | Settings | File Templates.
 */
public class SphereBuilder {
    public static Primitive build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();

        Vector3 pos = VectorBuilder.build((Node) xPath.evaluate("./pos", node, XPathConstants.NODE));
        float specularPow = Float.valueOf((String) xPath.evaluate("./radius/@r", node, XPathConstants.STRING));

        return new Sphere(specularPow, pos);
    }
}
