package cg.raytracing.builders.object.primitiv;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.primitives.Cylinder;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 06.03.13
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
public class CylinderBuilder {
    public static Cylinder build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        NodeList vertex = (NodeList) xPath.evaluate("./pos", node, XPathConstants.NODESET);
        float r = Float.valueOf((String) xPath.evaluate("./radius/@r", node, XPathConstants.STRING));
        Vector3 pos1 = VectorBuilder.build(vertex.item(0));
        Vector3 pos2 = VectorBuilder.build(vertex.item(1));
        return new Cylinder(pos1, pos2, r);
    }
}
