package cg.raytracing.builders.object.primitiv;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.primitives.Model;
import cg.raytracing.objects.primitives.SurfaceTriangle;
import cg.raytracing.objects.primitives.Triangle;
import cg.raytracing.stuff.Vector3;
import cg.raytracing.utils.Pair;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 17.03.13
 * Time: 2:21
 * To change this template use File | Settings | File Templates.
 */
public class ModelBuilder {
    public static Model build(Node node) throws XPathExpressionException, IOException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        Vector3 pos = VectorBuilder.build((Node) xPath.evaluate("./pos", node, XPathConstants.NODE));
        String file = (String)xPath.evaluate("./file/@name", node, XPathConstants.STRING);
        return read(file, pos);
    }
    private static Model read(String file, Vector3 pos) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        List<SurfaceTriangle> surfaces = new LinkedList<SurfaceTriangle>();
        List<Pair<int[], int[]>> delayed = new LinkedList<Pair<int[], int[]>>();
        float maxDist = 0;
        float dist;

        String strLine;
        List<Vector3> vertexList = new ArrayList<Vector3>(1000);
        List<Vector3> normal = new ArrayList<Vector3>(1000);
        String[] subs;
        try {
            int o = 0;
            while ((strLine = br.readLine()) != null) {
                subs = strLine.split("\\s+");
                if (subs != null && subs.length != 0) {
                    if ("v".equals(subs[0])) {
                        Vector3 v = new Vector3(Float.parseFloat(subs[1]), Float.parseFloat(subs[2]), Float.parseFloat(subs[3])).add(pos);
                        vertexList.add(v);
                        dist = v.getDistTo(pos);
                        if (dist > maxDist) {
                            maxDist = dist;
                        }
                    } else if ("vn".equals(subs[0])){
                        normal.add(new Vector3(Float.parseFloat(subs[1]), Float.parseFloat(subs[2]), Float.parseFloat(subs[3])));
                    } else if ("f".equals(subs[0])) {
    //                     vrt\tex\normal
                        int i1, i2, i3;
                        i1 = Integer.valueOf(subs[1].split("/")[0]) - 1;
                        i2 = Integer.valueOf(subs[2].split("/")[0]) - 1;
                        i3 = Integer.valueOf(subs[3].split("/")[0]) - 1;
                        int[] t = new int[]{i1, i2, i3};
                        try {
                            i1 = Integer.valueOf(subs[1].split("/")[2]) - 1;
                            i2 = Integer.valueOf(subs[2].split("/")[2]) - 1;
                            i3 = Integer.valueOf(subs[3].split("/")[2]) - 1;
                            delayed.add(new Pair<int[], int[]>(t, new int[]{i1, i2, i3}));
                        } catch (ArrayIndexOutOfBoundsException e) {
                            System.out.println("There are no normals in model file, please, select another one");
                            throw e;
                        }
                    }
                }

            }
        } finally {
            fis.close();
        }

        for (Pair<int[], int[]> p : delayed) {
            int[] t = p.getFirst();
            int[] n = p.getSecond();
            Vector3[] normals = new Vector3[]{normal.get(n[0]), normal.get(n[1]), normal.get(n[2])};
            Vector3[] vertexes = new Vector3[]{vertexList.get(t[0]), vertexList.get(t[1]), vertexList.get(t[2])};
            surfaces.add(new SurfaceTriangle(new Triangle(vertexes), normals));
        }

        return new Model(surfaces, pos, maxDist);
    }


}
