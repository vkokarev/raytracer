package cg.raytracing.builders.object.primitiv;

import cg.raytracing.builders.VectorBuilder;
import cg.raytracing.objects.primitives.Triangle;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 06.03.13
 * Time: 1:27
 * To change this template use File | Settings | File Templates.
 */
public class TriangleBuilder {
    public static Triangle build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        Vector3[] vertex = new Vector3[3];
        NodeList pos = (NodeList) xPath.evaluate("./pos", node, XPathConstants.NODESET);
        for (int i = 0; i < pos.getLength(); ++i) {
            vertex[i] = VectorBuilder.build(pos.item(i));
        }
        return new Triangle(vertex);
    }
}
