package cg.raytracing.builders.object;

import cg.raytracing.builders.object.primitiv.*;
import cg.raytracing.builders.object.primitiv.csg.CSGBuilder;
import cg.raytracing.objects.Object;
import cg.raytracing.objects.primitives.Primitive;
import cg.raytracing.stuff.Material;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 22:00
 * To change this template use File | Settings | File Templates.
 */
public class ObjectBuilder {
    public static Object build(Node node) throws XPathExpressionException, IOException {
        List<Primitive> primitives = new ArrayList<Primitive>();
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();

        Material material = MaterialBuilder.build((Node) xPath.evaluate("./material", node, XPathConstants.NODE));

        NodeList spheres = (NodeList) xPath.evaluate("./sphere", node, XPathConstants.NODESET);
        NodeList planes = (NodeList) xPath.evaluate("./plane", node, XPathConstants.NODESET);
        NodeList triangles = (NodeList) xPath.evaluate("./triangle", node, XPathConstants.NODESET);
        NodeList cylinders = (NodeList) xPath.evaluate("./cylinder", node, XPathConstants.NODESET);
        NodeList cones = (NodeList) xPath.evaluate("./cone", node, XPathConstants.NODESET);
        NodeList models = (NodeList) xPath.evaluate("./model", node, XPathConstants.NODESET);
        NodeList csg = (NodeList) xPath.evaluate("./csg", node, XPathConstants.NODESET);

        for (int i = 0; i < spheres.getLength(); ++i) {
            primitives.add(SphereBuilder.build(spheres.item(i)));
        }
        for (int i = 0; i < planes.getLength(); ++i) {
            primitives.add(PlaneBuilder.build(planes.item(i)));
        }
        for (int i = 0; i < triangles.getLength(); ++i) {
            primitives.add(TriangleBuilder.build(triangles.item(i)));
        }
        for (int i = 0; i < cylinders.getLength(); ++i) {
            primitives.add(CylinderBuilder.build(cylinders.item(i)));
        }
        for (int i = 0; i < cones.getLength(); ++i) {
            primitives.add(ConeBuilder.build(cones.item(i)));
        }
        for (int i = 0; i < models.getLength(); ++i) {
            primitives.add(ModelBuilder.build(models.item(i)));
        }

        for (int i = 0; i < csg.getLength(); ++i) {
            primitives.add(CSGBuilder.build(csg.item(i)));
        }
        return new Object(primitives, material);
    }
}
