package cg.raytracing.builders;

import cg.raytracing.objects.Camera;
import cg.raytracing.stuff.Vector3;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 26.02.13
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
class CameraBuilder {
    public static Camera build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        System.out.print("Creating camera... ");
        Vector3 pos = VectorBuilder.build((Node) xPath.evaluate("./pos", node, XPathConstants.NODE));
        Vector3 up = VectorBuilder.build((Node) xPath.evaluate("./up", node, XPathConstants.NODE));
        Vector3 lookAt = VectorBuilder.build((Node) xPath.evaluate("./look_at", node, XPathConstants.NODE));
        float fovX = Float.valueOf((String) xPath.evaluate("./fov_x/@angle", node, XPathConstants.STRING));
        float fovY = Float.valueOf((String) xPath.evaluate("./fov_y/@angle", node, XPathConstants.STRING));
        float distToNear = Float.valueOf((String) xPath.evaluate("./dist_to_near_plane/@dist", node, XPathConstants.STRING));
        System.out.println("[OK]");
        return new Camera(pos, lookAt, up, fovX, fovY, distToNear);
    }
}
