package cg.raytracing.builders;

import cg.raytracing.utils.GlobalSettings;
import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 02.04.13
 * Time: 22:57
 * To change this template use File | Settings | File Templates.
 */
public class GlobalSettingsBuilder {
    public static void build(Node node) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
        boolean refractionHack = Boolean.parseBoolean((String)xPath.evaluate("./refraction-hack/@enable", node, XPathConstants.STRING));
        boolean gammaCorrection = Boolean.parseBoolean((String)xPath.evaluate("./gamma-correction/@enable", node, XPathConstants.STRING));
        boolean schlickApproximation = Boolean.parseBoolean((String)xPath.evaluate("./schlick-approximation/@enable", node, XPathConstants.STRING));

        System.out.println("Global settings\n\tRefarction hack : " + (refractionHack?"on":"off") + " \n\tGamma correction : " + (gammaCorrection?"on":"off") +
                " \n\tSgclick aproximation : " + (schlickApproximation?"on":"off"));

        GlobalSettings.REFRACTION_HACK = refractionHack;
        GlobalSettings.GAMMA_CORRECTION = gammaCorrection;
        GlobalSettings.SCHLICK = schlickApproximation;

    }
}
