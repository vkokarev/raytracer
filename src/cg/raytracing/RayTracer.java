package cg.raytracing;

import cg.raytracing.builders.WorldBuilder;
import cg.raytracing.objects.Camera;
import cg.raytracing.objects.Object;
import cg.raytracing.objects.Scene;
import cg.raytracing.objects.World;
import cg.raytracing.objects.lights.Light;
import cg.raytracing.objects.primitives.Primitive;
import cg.raytracing.output.Image;
import cg.raytracing.stuff.*;
import cg.raytracing.utils.GlobalSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.13
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
public class RayTracer {
    private static final int PARTS = 10;
    private static final int MAX_DEPTH = 4;
    private World world;
    private Scene scene;
    private Camera camera;
    private static int finished;

    public RayTracer(String fileName) throws Exception {
        File file = new File(fileName);
        try {
            world = WorldBuilder.build(file);
        } catch (Exception e) {
            System.out.println("Exception [" + e.getClass().getSimpleName() + "] accured while creating the world from xml! Message : " + e.getMessage());
            throw e;
        }
        scene = world.getScene();
        camera = world.getCamera();

    }

    public void render(String output, final int w, final int h, int threads) throws Exception {
        final Image image = new Image(w, h);
        ExecutorService service = Executors.newFixedThreadPool(threads);

        System.out.println();

        int stepW = w / PARTS;
        int stepH = h / PARTS;
        for (int i = 0; i < PARTS; ++i) {
            final int startI = i * stepW;
            final int finishI = Math.min(w, (i + 1) * stepW);
            for (int j = 0; j < PARTS; ++j) {
                final int startJ = j * stepH;
                final int finishJ = Math.min((j + 1) * stepH, h);
                service.submit(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = startI; i < finishI; ++i) {
                            for (int j = startJ; j < finishJ; ++j) {
                                Ray ray = null;
                                try {
                                    ray = getRay(w, h, i, j);
                                } catch (Exception e) {
                                    System.out.println("Unexpected exception while rendering");
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                                Color color = new Color(0, 0, 0);
                                Intersection intersection = findIntersection(ray);
                                if (intersection.isValid()) {
                                    color = getColor(ray, intersection);
                                    image.setPixel(i, j, color.normolize());
                                }
                            }
                        }
                        finished++;
                        System.out.print("\raproximatly " + ((100. * finished) / (PARTS * PARTS)) + "% finished");
                    }
                });
            }
        };
        service.shutdown();
        if (!service.awaitTermination(5, TimeUnit.HOURS)) {
            service.shutdownNow();
            if (!service.awaitTermination(1, TimeUnit.HOURS)) {
                System.err.println("ScheduledCalculate pool did not terminate.");
            }
        }
        System.out.println("\r100% finished");
        image.save(output);
    }

    public void render(String output, int w, int h) throws Exception {
        Image image = new Image(h, w);

        for (int i = 0; i < w; ++i) {
            for (int j = 0; j < h; ++j) {
                Ray ray = getRay(w, h, i, j);
                Color color = new Color(0, 0, 0);
                Intersection intersection = findIntersection(ray);
                if (intersection.isValid()) {
                    color = getColor(ray, intersection);
                    Vector3 p = new Vector3(world.getPv().mult(intersection.getIntersectionPoint()));
                    image.setPixel(i, j, color.normolize());
                }
            }
        }
        image.save(output);
    }


    public Ray constructRayThroughPixel(int x, int y, int h, int w) throws Exception {
        Ray ray = new Ray(camera.getPos(), camera.getDir());
        Vector3 endPoint = ray.getScaledEndPoint(1000);

        float upOffset = -1 * (y - (h / 2));
        float rightOffset = (x - (w / 2));

        endPoint = camera.getViewplaneUp().mult(upOffset).add(endPoint);
        endPoint = camera.getRightDirection().mult(rightOffset).add(endPoint);

        ray.setDir(endPoint.minus(camera.getPos()).getDir());
        return ray;
    }

    private Ray getRay(int h, int w, int x, int y) throws Exception {
        Ray ray = new Ray(camera.getPos(), camera.getDir());
        Vector3 endPoint = ray.getScaledEndPoint(1000);

        float upOffset = -1 * (y - (h / 2));
        float rightOffset = (x - (w / 2));

        endPoint = camera.getViewplaneUp().mult(upOffset).add(endPoint);
        endPoint = camera.getRightDirection().mult(rightOffset).add(endPoint);

        ray.setDir(endPoint.minus(camera.getPos()).getDir());
        return ray;
    }

    private Intersection findIntersection(Ray ray) {
        return findIntersection(ray, null);
    }

    private Intersection findIntersection(Ray ray, List<Primitive> excluded) {
        List<Object> objects = scene.getObjects();
        Intersection intersection = new Intersection(null, null, ray, Float.POSITIVE_INFINITY);
        for (Object object : objects) {
            Intersection local = object.isIntersect(ray, excluded);
            if (local.isValid() && local.getDist() < intersection.getDist()) {
                intersection = local;
            }
        }
        return intersection;
    }

    private Color getColor(Ray ray, Intersection intersection) {
        return getColor(ray, intersection, 0, new ArrayList<Primitive>(), 1);
    }

    private Color getColor(Ray ray, Intersection intersection, int depth, List<Primitive> excluded, float curRefraction) {

        if (depth >= MAX_DEPTH) {
            return new Color(0, 0, 0);
        }

        Material material = intersection.getObject().getMaterial();

        Vector3 intersectionPoint = intersection.getIntersectionPoint();

        Vector3 normal = intersection.getPrimitive().getNormal(intersectionPoint);

        List<Light> lights = scene.getLights();

        Color color = new Color(0, 0, 0);

        for (Light light : lights) {
            Ray toLight = light.rayFromPoint(intersectionPoint);
            if (toLight == null)
                continue;
            toLight.normolize();
            toLight.setPosition(toLight.getPosition().add(toLight.getDir().mult(GlobalSettings.SUPERMAGICEPSILONCONSTANTTHATHASREALLYBIGNAME)));
            Intersection intersectionWithObject = findIntersection(toLight, excluded);
            float distToIntersection = Float.POSITIVE_INFINITY;
            if (intersectionWithObject.isValid())
                distToIntersection = intersectionWithObject.getDist();

            float distToLight = toLight.getPosition().getDistToZero();

            boolean inShadow = distToLight > distToIntersection;

            if (!inShadow) {
                Color coeffColor = light.getCoeff(intersectionPoint);
                Vector3 coeff = new Vector3(coeffColor.getAsArray());

                float diffuseCoeff = toLight.getDir().mult(normal);
                if (diffuseCoeff > 0) {
                    Vector3 diffuse = new Vector3(material.getDiffuse().getAsArray());
                    diffuse = diffuse.mult(diffuseCoeff).multComponent(coeff);
                    color = color.add(Color.valueOf(diffuse));
                }

                Vector3 reflected = toLight.getReflected(normal);
                float reflectionCoeff = reflected.getDir().mult(normal);
                if (reflectionCoeff < 0) {
                    float specularCoeff = (float) Math.pow(-reflectionCoeff, material.getSpecularPow());
                    Vector3 specular = new Vector3(material.getSpecular().getAsArray());
                    specular = specular.mult(specularCoeff).multComponent(coeff);
                    color = color.add(Color.valueOf(specular));
                }

                Vector3 ambientScene = new Vector3(material.getAmbient().getAsArray());
                Vector3 ambientLight = new Vector3(light.getAmbient().getAsArray());
                color = color.add(Color.valueOf(ambientScene.multComponent(ambientLight)));
            }
        }

        if (material.reflects()) {
            Ray refl = new Ray(intersectionPoint, ray.getDir().mult(-1));
            Vector3 reflected = refl.getReflected(normal);
            refl.setDir(reflected);
            refl.setPosition(refl.getPosition().add(reflected.mult(GlobalSettings.SUPERMAGICEPSILONCONSTANTTHATHASREALLYBIGNAME)));
            Intersection intersectionReflect = findIntersection(refl, excluded);
            if (intersectionReflect != null && intersectionReflect.isValid()) {
                Color reflectionColor = getColor(refl, intersectionReflect, depth + 1, excluded, curRefraction);
                reflectionColor.mult((float)Math.pow(material.getReflectionFactor(), depth + 1));
                color = color.add(reflectionColor);
            }
        }

        if (material.refracts()) {
            float cosTheta = -normal.mult(ray.getDir());
            float n = material.getRefractionCoeff();
            n = curRefraction / n;
            if (cosTheta < 0) {
                cosTheta *= -1;
                normal = normal.mult(-1);
            }
            float schlick = 0;
            if (GlobalSettings.SCHLICK) {
                float r0;
                r0 = ((n - 1) / (n + 1));
                r0 *= r0;
                float dirDotNormal = 1 + ray.getDir().mult(normal);
                schlick = (r0 + (1 - r0) * dirDotNormal * dirDotNormal * dirDotNormal * dirDotNormal * dirDotNormal);
            }

            float k = 1 - n * n * (1 - cosTheta * cosTheta);

            if (k > 0) {
                //System.out.println(schlick + "\t" + k + "\t" + Math.sqrt(k) + "\t" + (n * cosTheta - (float)Math.sqrt(k)));
                Vector3 dir;
                if (GlobalSettings.SCHLICK) {
//                    dir = ray.getDir().add(normal.mult(n * cosTheta - (float)Math.sqrt(k))).getDir();
                    dir = ray.getDir().add(normal.mult(-schlick)).getDir();
                } else {
                    dir = ray.getDir().add(normal.mult(n * cosTheta - (float)Math.sqrt(k))).getDir();
                }
                Ray refRay = new Ray(intersectionPoint.add(dir.mult(GlobalSettings.SUPERMAGICEPSILONCONSTANTTHATHASREALLYBIGNAME)), dir);
                Intersection refractInt = findIntersection(refRay, excluded);
                if (refractInt.isValid()) {
                    Color ref;
                    if (GlobalSettings.REFRACTION_HACK && (depth > 1)) {
                        excluded.add(intersection.getPrimitive());
                        ref = getColor(refRay, refractInt, depth + 1, excluded, material.getRefractionCoeff());
                        excluded.remove(intersection.getPrimitive());
                    } else {
                        ref = getColor(refRay, refractInt, depth + 1, excluded, material.getRefractionCoeff());
                    }
                    Color absorbance = new Color(color.getR(), color.getG(), color.getB());
                    absorbance.mult(-0.15f * refractInt.getDist());
                    Color transparency = new Color((float)Math.exp(absorbance.getR()), (float)Math.exp(absorbance.getG()), (float)Math.exp(absorbance.getB()));
                    color = color.add(new Color(transparency.getR() * ref.getR(), transparency.getG() * ref.getG(), transparency.getB() * ref.getB()));
                }
            }
        }
        color.norm();
        return color;
    }

    public static void main(String... args) throws Exception {
        //--scene=my_scene.xml --resolution_x=30 --resolution_y=42 --output=my_scene.png
        String scenePath = null;
        Integer x = null, y = null;
        String out = null;
        for (String arg : args) {
            if (arg.startsWith("--scene")) {
                scenePath = arg.substring("--scene=".length(), arg.length());
            } else if (arg.startsWith("--output")) {
                out = arg.substring("--output=".length(), arg.length());
            } else if (arg.startsWith("--resolution_x")) {
                x = new Integer(arg.substring("--resolution_x=".length(), arg.length()));
            } else if (arg.startsWith("--resolution_y")) {
                y = new Integer(arg.substring("--resolution_y=".length(), arg.length()));
            }
        }
        System.out.println("Input params :\n" +
                "\tscene : " + scenePath + "\n" +
                "\toutput : " + out + "\n" +
                "\tres_x : " + x + "\tres_y : " + y
        );
//        RayTracer rayTracer = new RayTracer("C:\\Users\\1\\Dropbox\\cg\\obj.xml");
//        RayTracer rayTracer = new RayTracer("/home/user/Dropbox/cg/scene2.xml");
        RayTracer rayTracer = new RayTracer(scenePath);
        System.out.println("---------------------Time Test-------------------");
        int th = 8; //а это что за магия???
        long start = System.currentTimeMillis();
        rayTracer.render(out, x, y, th);
        long end = System.currentTimeMillis();
        System.out.println(": took@" + (end - start) + " ms");
        System.out.println("-----------------Time Test Finish----------------");
    }


}

