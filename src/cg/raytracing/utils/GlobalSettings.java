package cg.raytracing.utils;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 02.04.13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */
public class GlobalSettings {
    public static boolean REFRACTION_HACK = true;
    public static boolean GAMMA_CORRECTION = false;
    public static boolean SCHLICK = false;
    public static final float SUPERMAGICEPSILONCONSTANTTHATHASREALLYBIGNAME = (float) 1e-3;
}
